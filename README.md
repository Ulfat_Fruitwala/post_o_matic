# Post_O_Matic
--------------
Post_O_Matic is a web application that provides mini social media interface to interact with other users. A user can create posts which will be visible to other users who then can reply to the post.

## Getting Started
------------------
These instructions will get you a copy of the project up and running on your local machine for development and testing.

Prerequisites
-------------
-Python 3

-PY4WEB: Installing from pip
```bash
py4web setup apps
py4web set-password
py4web run apps
```
After installing py4web, your application can be accessed at the following urls:
```bash
http://localhost:8000
http://localhost:8000/_dashboard
http://localhost:8000/{yourappname}/index
```

Installing
----------
Go to the apps folder in your py4web directory on your local machine
```bash
cd py4web/apps
git clone https://Ulfat_Fruitwala@bitbucket.org/Ulfat_Fruitwala/post_o_matic.git
```
The application can be accessed at the following url
```bash
http://localhost:8000/post_o_matic/index
```


## Usage
--------
- Sign up for an account accordingly
- Add a new post by clicking on 'Add Post' button
- After a post is added, it can be edited by clicking on the yellow edit button below it
- To reply to a post, click on the blue reply button
- Each post and reply has its author below it
- Posts cannot be deleted, only replies can be deleted

## Built Using
--------------
- py4web
- vue.js
- python
- javascript
- html/css

## Author
---------
- Ulfat Fruitwala